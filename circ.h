
typedef struct{
    int * data; 
    int begin;
    int end;
    int capacity; //max de elementos
    int size;//qtd de elementos
}Deque;

Deque * deque_create(int capacity);
void deque_destroy(Deque * deque);

int deque_get_size(Deque * deque);
int deque_get_capacity(Deque * deque);

//insere chave no fim
void deque_push_back (Deque * deque, int value);
//insere chave no comeco
void deque_push_front(Deque * deque, int value);

//retira chave do fim
void deque_pop_back  (Deque * deque);
//retira chave do comeco
void deque_pop_front (Deque * deque);

//endereco do ultimo
int * deque_back(Deque * deque);
//endereco do primeiro
int * deque_front(Deque * deque);

//preenche values com os valores do deque e em qtd coloca a quantidade
//de elementos ate o limite de max
void deque_fill_values(Deque * deque, int values[], int * qtd, int max);

