#include <stdio.h>
#include "lib/vector.h"

void show(Vector * v){
    float * it;
    for(it = vector_front(v); it != vector_end(v); it++)
        printf("%.1f ",*it);
    puts("");
}

int main(){

    Vector * v = vector_create(10, sizeof(float));
    show(v);
    int i;
    for(i = 0; i < 21; i++){
        float f = i % 7 + 0.1;
        vector_push_back(v, &f);
    }
    puts("preenchi de 0.1 a 6.1, 21 numeros");
    show(v);

    printf("size %d\n", vector_size(v));
    printf("capacity %d\n", vector_capacity(v));

    puts("coloquei 0 no primeiro 5.1");
    float f = 5.1;
    float * p = vector_find(v, NULL, &f);
    *p = 0.0;
    show(v);

    puts("removi o primeiro 4.1 e coloquei 0 no proximo dele");
    /*f = 4.1;*/
    p = vector_remove(v, NULL, &(float) {4.1});
    *p = 0;

    show(v);

    f = 1.1;
    vector_remove_all(v, &f);
    f = 2.1;
    vector_remove_all(v, &f);
    puts("removi todos os 1.1s e 2.1s");
    show(v);

    return 0;
}
