#include <stdio.h>
#include "vector.h"

void show(Vector * v){
    int * it;
    for(it = vector_front(v); it != vector_end(v); it++)
        printf("%d ",(int) *it);
    puts("");
}

int main(){

    Vector * v = vector_create(10);
    show(v);
    int i;
    for(i = 0; i < 20; i++){
        vector_push_back(v, i % 7);
    }
    puts("preenchi de 0 a 6, 20 numeros");
    show(v);

    printf("size %d\n", vector_size(v));
    printf("capacity %d\n", vector_capacity(v));

    puts("coloquei 0 no primeiro 5");
    int * p = vector_find(v, NULL, 5);
    *p = 0;
    show(v);

    puts("removi o primeiro 4 e coloquei 0 no proximo dele");
    p = vector_remove(v, NULL, 4);
    *p = 0;

    show(v);

    vector_remove_all(v, 1);
    puts("removi todos os 1s");
    show(v);

    return 0;
}
