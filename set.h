
enum {FALSE = 0, TRUE = 1} boolean;

//implementacao com vetor
#define SET_VECCTOR
//#define SET_LIST
//#define SET_TREE

#ifdef SET_VECTOR
typedef struct{
    int * data;
    int size;
    int capacity;
}Set;
#endif

#ifdef SET_LIST
struct Set;

typedef struct{
    int value;
    Set * next;
}Set;

typedef Set Node;
#endif

#ifdef SET_LIST
struct Set;

typedef struct{
    int value;
    Tree * left;
    Tree * right;
}Tree;

typedef Set Tree;
#endif

Set * set_create(int capacity);
Set * set_create_by_copy(Set * set);
void  set_destroy(Set * set);

size_t set_get_size(Set * set);

//insere chave, se ela já nao existir, realocando se precisar
//retorna 1 se operacao bem sucedida
boolean set_add (Set * set, int value);

//retira chave se existir e retorna 1
boolean set_remove  (Set * set, int value);

//retorna true se existe esse valor no conjunto
boolean set_exists(Set * set, int value);

//preenche values com os valores das chaves
void set_get_values(Set * set, int values[], int max);

//retorna true se A eh subconjunto de B
boolean set_is_subset(Set * A, Set * B);

//cria um conjunto baseado na uniao
Set * set_create_by_union(Set * A, Set * B);

//cria um conjunto baseado na uniao
Set * set_create_by_subtraction(Set * A, Set * B);

//cria um conjunto baseado na intercessao
Set * set_create_by_intersection(Set * A, Set * B);
