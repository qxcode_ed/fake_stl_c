#include <stdio.h>
#include "lib/vector.h"
#include <string.h>

void show(Vector * v){
    char ** it;
    printf("[ ");
    for(it = vector_front(v); it != vector_end(v); it++)
        printf("%s ",*it);
    puts("]");
}

void insert(Vector * v){
    char *p = "bax";
    vector_push_back(v, &p);
    p = "ce";
    vector_push_back(v, &p);
    p = "pim";
    vector_push_back(v, &p);
    p = "bax";
    vector_push_back(v, &p);
    p = "del";
    vector_push_back(v, &p);
    p = "ww";
    vector_push_back(v, &p);
    p = "rex";
    vector_push_back(v, &p);
    p = "ww";
    vector_push_back(v, &p);
    p = "rex";
    vector_push_back(v, &p);
    p = "bax";
    vector_push_back(v, &p);
}

int mystrcmp(const void *a, const void *b){
    return strcmp((char*)a, (char*)b);
}

int main(){

    Vector * v = vector_create(1, sizeof(char *));
    vector_set_cmp(v, mystrcmp);
    insert(v);

    show(v);
    puts("preenchi com 7 palavras");

    show(v);

    printf("size %d\n", vector_size(v));
    printf("capacity %d\n", vector_capacity(v));

    puts("coloquei pegasus no primeiro ovo");
    char *str = "rex";
    void ** p = vector_find(v, NULL, &str);
    if(p == NULL)
        return 0;
    *p = "pegasus";
    show(v);

    puts("removi o primeiro del e coloquei ninja no proximo dele");
    str = "del";
    p = vector_remove(v, NULL, &str);
    *p = "ninja";

    show(v);
    str = "ww";
    vector_remove_all(v, &str);
    str = "bax";
    vector_remove_all(v, &str);
    puts("removi todos os ww e bax");
    show(v);

    return 0;
}
