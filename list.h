struct Node;

typedef struct{
    int data;
    Node * next;
    Node * prev;
}Node;

//avanca pra frente ou pra trás enquanto nao for nulo
Node * node_advance(Node * node, int steps);
//aloca e retona um no com a chave
Node * node_create(int value);
//apaga valores e destroi esse node
void node_destroy(Node * node);

//************************************************************

typedef struct{
    Node * head;
    Node * tail;
    int size;
}List;

List * list_create();
void list_destroy(List * list);

//insere chave no fim
void list_push_back (List * list, int value);
//insere chave no comeco
void list_push_front(List * list, int value);

//retira chave do fim
void list_pop_back  (List * list);
//retira chave do comeco
void list_pop_front (List * list);

//endereco do ultimo
Node * list_back(List * list);
//endereco do primeiro
Node * list_front(List * list);

//encontra e retorna o proximo elemento a partir da posicao de node
Node * list_find(List * list, Node * node, int value);

//remove o no com esse endereco da lista
void list_remove    (List * list, Node * node); 
void list_remove_all(List * list, int value); 

//insere chave antes do no
void list_insert(List * list, Node * node, int value);
