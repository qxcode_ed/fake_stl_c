struct Node;

typedef struct{
    void * data;
    Node * next;
    Node * prev;
}Node;

//avanca pra frente ou pra trás enquanto nao for nulo
Node * node_advance(Node * node, int steps);

//aloca, copia e retona um no com a copia da chave
Node * node_create(void * data, size_t size_elem);

//apaga data e destroi node e data 
void node_destroy(Node * node);

//###################################

typedef struct{
    Node * head;
    Node * tail;
    int size;
    size_t size_elem;
}List;

List * list_create(size_t size_elem);
void list_destroy(List * list);

//insere chave no fim
void list_push_back (List * list, void * value);
//insere chave no comeco
void list_push_front(List * list, void * value);

//retira chave do fim
void list_pop_back  (List * list);
//retira chave do comeco
void list_pop_front (List * list);

//endereco do ultimo
Node * list_back(List * list);
//endereco do primeiro
Node * list_front(List * list);
//endereco depois do ultimo
Node * list_end(List * list);

//encontra e retorna o proximo elemento a partir da posicao de node
Node * list_find(List * list, Node * node, void * value);

//remove o no com esse endereco da lista
void list_remove    (List * list, Node * node); 
void list_remove_all(List * list, void * value); 

//Cria um novo no com a chave value
//Insere o novo nó antes de node e atualiza head se necessário
void list_insert(List * list, Node * node, void * value);
