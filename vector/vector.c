#include <stdlib.h>
#include "vector.h"

Vector * vector_create(int capacity){
    Vector * vec = malloc(sizeof(Vector));
    vec->data = malloc(sizeof(int) * capacity);
    vec->size = 0;
    vec->capacity = capacity;
    return vec;
}

void vector_destroy(Vector * vector){
    free(vector->data);
    free(vector);
}

int vector_size(Vector * vector){
    return vector->size;
}

int vector_capacity(Vector * vector){
    return vector->capacity;
}

void vector_reserve(Vector * vector, int capacity){
    int * data = malloc(capacity * sizeof(int));

    vector->capacity = capacity;  

    //se nova capacidade for menor
    if(capacity < vector->size)
        vector->size = capacity;

    int i = 0;
    for(i = 0; i < vector->size; i++){
        data[i] = vector->data[i];
    }
    free(vector->data);
    vector->data = data;
}

void vector_push_back (Vector * vector, int value){
    if(vector->size == vector->capacity)
        vector_reserve(vector, 2 * vector->capacity);

    vector->data[vector->size] = value;
    vector->size += 1;
}

void vector_pop_back  (Vector * vector){
    vector->size -= 1;
}


int * vector_back(Vector * vector){
    return &vector->data[vector->size - 1];    
}

int * vector_end(Vector * vector){
    return &vector->data[vector->size];    
}


int * vector_front(Vector * vector){
    return &vector->data[0];
}


int * vector_find(Vector * vector, int * it, int value){
    if(it == NULL)
        it = &vector->data[0];
     
    int * end = &vector->data[vector->size];
    for(;it != end; it++){
        if(*it == value)
            return it;
    }
    return NULL;
}

void vector_remove_it(Vector * vector, int * it){
    int * end = &vector->data[vector->size];
    for(;it + 1 < end; it++){
        *it = *(it + 1);
    }
    vector->size -= 1;
}


int * vector_remove(Vector * vector, int * it, int value){
    if(it == NULL)
        it = &vector->data[0];
    int * end = &vector->data[vector->size];

    for(;it < end; it++){
        if(*it == value){
            vector_remove_it(vector, it);
            return it;
        }
    }
    return NULL;
}
    
void vector_remove_all(Vector * vector, int value){
    int * rend = &vector->data[vector->size] - 1;
    int * rbegin = &vector->data[0] - 1;
    int * it;
    for(it = rend; it > rbegin; it--){
        if(*it == value){
            vector_remove_it(vector, it);
        }
    }
}

void vector_insert(Vector * vector, int * it, int value){
    int * end = &vector->data[vector->size];
    vector_push_back(vector, value);

    if(it == NULL)
        it = &vector->data[0];

    int *aux = it;
    for(;it + 1 < end; it++){
        *(it + 1) = *it;
    }
    *aux = value;
}
