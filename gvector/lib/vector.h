#ifndef  VECTOR_H
#define VECTOR_H
#include <stdio.h>

typedef struct{
    void * data;
    size_t size_one;
    int size;
    int capacity;
    int (*fn_cmp)(const void *, const void *);
}Vector;

//Cria um vetor dinamicamente definindo a capacidade .
//Retorna o endereço do vector mallocado. 
Vector * vector_create(int capacity, size_t size_one);

//retorna data e vector
void vector_destroy(Vector * vector);

int vector_size(Vector * vector);
int vector_capacity(Vector * vector);

//aloca a capacidade para o vetor, 
//se for mais que o atual, pede mais memoria
//e copia os dados
//se for menos pede menos memoria 
void vector_reserve(Vector * vector, int capacity);

//insere elem no fim, realoca se necessario duplicando o tamanho
//insere copiando o valor da chave
void vector_push_back (Vector * vector, const void * elem);

//retira chave do fim, diminuindo o tamanho
void vector_pop_back  (Vector * vector);

//endereco do elemento de posicao pos
void * vector_at(Vector * vector, int pos);

//endereco do ultimo
void * vector_back(Vector * vector);

//endereco do primeiro
void * vector_front(Vector * vector);

//endereco da posicao apos o ultimo
void * vector_end(Vector * vector);

//encontra e retorna o proximo elemento a partir da posicao de it
//se it == NULL inicia no começo
//retorna null se nao encontrar
//verifica a igualdade por comparacao dos bits de elem com os do vetor
void * vector_find(Vector * vector, void * it, const void * elem);

//Remove o primeiro elemento a partir de it 
//if it == NULL entao começa do primeiro elemento do vetor
//retorna o endereco onde o elemento estava ou NULL
void * vector_remove (Vector * vector, void * it, const void * elem); 

//remove todos os elementos iguais a elem
void vector_remove_all(Vector * vector, const void * elem); 

//if it == NULL entao começa do primeiro elemento do vetor
//copia o valor de elem na posicao atual deslocando tudo pra frente 
void vector_insert(Vector * vector, void * it, const void * elem);


//remove todos os elementos que retornarem positivo para funcao
void vector_remove_if(Vector * vector, int (*fn) (const void *)); 

//A funcao de comparação normal é bit a bit.
//Essa função muda a função de comparação 
//Se passar fn NULL então retorna a funcao de comparacao normal
void vector_set_cmp(Vector * vector, int (*fn)(const void *, const void *));
#endif//  VECTOR_H
