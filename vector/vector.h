#ifndef  VECTOR_H
#define VECTOR_H
#include <stdio.h>

typedef struct{
    int * data;
    int size;
    int capacity;
}Vector;

//Cria um vetor dinamicamente definindo a capacidade .
//Retorna o endereço do vector mallocado. 
Vector * vector_create(int capacity);

//retorna data e vector
void     vector_destroy(Vector * vector);

int vector_size(Vector * vector);
int vector_capacity(Vector * vector);

//aloca a capacidade para o vetor, 
//se for mais que o atual, pede mais memoria
//e copia os dados
//se for menos pede menos memoria 
void vector_reserve(Vector * vector, int capacity);

//insere chave no fim, realoca se necessario duplicando o tamanho
void vector_push_back (Vector * vector, int value);

//retira chave do fim, diminuindo o tamanho
void vector_pop_back  (Vector * vector);

//endereco do ultimo
int * vector_back(Vector * vector);

//endereco do primeiro
int * vector_front(Vector * vector);

//endereco da posicao apos o ultimo
int * vector_end(Vector * vector);

//encontra e retorna o proximo elemento a partir da posicao de it
//se it == NULL inicia no começo
//retorna null se nao encontrar
int * vector_find(Vector * vector, int * it, int value);

//remove o primeiro elemento a partir de it 
//if it == NULL entao começa do primeiro elemento do vetor
//retorna o endereco onde o elemento estava ou NULL
int * vector_remove   (Vector * vector, int * it, int value); 
//remove todos os elementos com esse valor
void vector_remove_all(Vector * vector, int value); 

//if it == NULL entao começa do primeiro elemento do vetor
//insere chave na posicao atual deslocando tudo pra frente 
void vector_insert(Vector * vector, int * it, int value);

#endif//  VECTOR_H
