


enum {FALSE = 0, TRUE = 1} boolean;

typedef struct{
    const char * key;
    void * data;
    Map * left;
    Map * right;
}Map;

Map * map_create();
void map_destroy(Map * map);

//se nao existir a chave, insira e retorne TRUE
boolean map_insert(Map * map, const char * key, void * data);

//retorna o dado associado a key ou NULL
void * map_get(Map * map, const char * key);

//retorna o dado associado a key e remove a entrada do mapa
void * map_remove(Map * map, const char * key);

int map_size(Map * map);

void map_fill_keys(Map * map, char ** keys);
