#include <stdio.h>
#include "lib/vector.h"
#include <string.h>

typedef struct{
    int existe;
    char key;
}Elem;

void show(Vector * v){
    printf("size %d, ", vector_size(v));
    printf("[");
    int i;
    for(i = 0; i < vector_size(v); i++){
        Elem * el = vector_at(v, i);
        printf(" %d.%c ", el->existe, el->key);
    }
    puts("]");
}

void insert(Vector * v){
    Elem p = (Elem){1, 'a'};
    vector_push_back(v, &p);
    p = (Elem){0, 'b'};
    vector_push_back(v, &p);
    p = (Elem){0, 'c'};
    vector_push_back(v, &p);
    p = (Elem){0, 'd'};
    vector_push_back(v, &p);
    p = (Elem){1, 'e'};
    vector_push_back(v, &p);
    p = (Elem){1, 'f'};
    vector_push_back(v, &p);
    p = (Elem){1, 'i'};
    vector_push_back(v, &p);
}

int test_nao_existe(const void *a){
    return (((Elem*)a)->existe == 0);
}

int cmpbykey(const void *a, const void *b){
    if(((Elem*)a)->key == ((Elem*)b)->key)
        return 0;
    return 1;
}

int main(){

    Vector * v = vector_create(1, sizeof(char *));
    vector_set_cmp(v, cmpbykey);
    insert(v);

    puts("preenchi com 7 palavras");
    show(v);

    printf("capacity %d\n", vector_capacity(v));

    puts("coloquei 1.c no primeiro 0.c");
    Elem elem = {1, 'c'};
    Elem * p = vector_find(v, NULL, &elem);
    if(p == NULL)
        return 0;
    p->existe = 1;
    show(v);

    puts("removi o primeiro b e coloquei 1.z no proximo dele");
    /*elem = (Elem){0, 'b'};*/
    //passando o endereco com objeto anonimo
    p = vector_remove(v, NULL, &(Elem){0, 'b'});
    *p = (Elem){1, 'z'}; 

    show(v);
    puts("removendo todos os que nao existem");
    vector_remove_if(v, test_nao_existe);
    show(v);

    return 0;
}
