#include <stdlib.h>
#include <memory.h>
#include "vector.h"

Vector * vector_create(int capacity, size_t size_one){
    Vector * vec = malloc(sizeof(Vector));
    vec->fn_cmp = NULL;
    vec->data = malloc(size_one * capacity);
    vec->size = 0;
    vec->size_one = size_one;
    vec->capacity = capacity;
    return vec;
}

void vector_destroy(Vector * vector){
    free(vector->data);
    free(vector);
}

int vector_size(Vector * vector){
    return vector->size;
}

int vector_capacity(Vector * vector){
    return vector->capacity;
}

void vector_reserve(Vector * vector, int capacity){
    void * data = malloc(capacity * vector->size_one);
    vector->capacity = capacity;  
    //se nova capacidade for menor
    if(capacity < vector->size)
        vector->size = capacity;
    memcpy(data, vector->data, vector->size * vector->size_one);
    free(vector->data);
    vector->data = data;
}



void vector_push_back (Vector * vector, const void * elem){
    if(vector->size == vector->capacity)
        vector_reserve(vector, 2 * vector->capacity);

    void * pos = vector->data + vector->size * vector->size_one;
    memcpy(pos, elem, vector->size_one);
    vector->size += 1;
}

void vector_pop_back  (Vector * vector){
    vector->size -= 1;
}

void * vector_at(Vector * vector, int pos){
    return vector->data + pos * vector->size_one;
}

void * vector_back(Vector * vector){
    return vector_end(vector) - vector->size_one;    
}

void * vector_end(Vector * vector){
    return vector->data + vector->size * vector->size_one;    
}

void * vector_front(Vector * vector){
    return vector->data;
}

int vector_is_equal(Vector * vector, void * it, const void * elem){
    if(vector->fn_cmp == NULL)
        return (memcmp(it, elem, vector->size_one) == 0);
    return (vector->fn_cmp(it, elem) == 0);
}


void * vector_find(Vector * vector, void * it, const void * elem){
    size_t s_one = vector->size_one;
    if(it == NULL)
        it = vector->data;
     
    void * end = vector_end(vector);
    for(;it < end; it += s_one){
        if(vector_is_equal(vector, it, elem))
            return it;
    }
    return NULL;
}

void vector_remove_it(Vector * vector, void * it){
    size_t s_one = vector->size_one;
    void * end = vector_end(vector);
    for(;it + s_one < end; it += s_one){
        memcpy(it,it + s_one, s_one);
    }
    vector->size -= 1;
}

void * vector_remove(Vector * vector, void * it, const void * elem){
    size_t s_one = vector->size_one;
    if(it == NULL)
        it = vector->data;
    void * end = vector_end(vector);

    for(;it < end; it += s_one){
        if(vector_is_equal(vector, it, elem)){
            vector_remove_it(vector, it);
            return it;
        }
    }
    return NULL;
}
    
void vector_remove_all(Vector * vector, const void * elem){
    size_t s_one = vector->size_one;
    void * rend = vector_back(vector); 
    void * rbegin = vector->data - s_one;
    void * it;
    for(it = rend; it > rbegin; it -= s_one){
        if(vector_is_equal(vector, it, elem)){
            vector_remove_it(vector, it);
        }
    }
}

//remove todos os elementos que retornarem positivo para funcao
void vector_remove_if(Vector * vector, int (*fn) (const void *)){
    size_t s_one = vector->size_one;
    void * rend = vector_back(vector); 
    void * rbegin = vector->data - s_one;
    void * it;
    for(it = rend; it > rbegin; it -= s_one){
        if(fn(it)){
            vector_remove_it(vector, it);
        }
    }

}

void vector_insert(Vector * vector, void * it, const void * elem){
    size_t s_one = vector->size_one;
    void * end = vector_end(vector);
    vector_push_back(vector, elem);

    if(it == NULL)
        it = vector->data;

    void *aux = it;
    for(;it + s_one < end; it += s_one){
        memcpy(it + s_one, it, s_one);
    }
    memcpy(aux, elem, s_one);
}

void vector_set_cmp(Vector * vector, int (*fn)(const void *, const void *)){
    vector->fn_cmp = fn;
}
